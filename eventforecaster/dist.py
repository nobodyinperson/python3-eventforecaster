# system modules

# internal modules

# external modules
import scipy.interpolate
import scipy.signal
import numpy as np
import statsmodels.api as sm


class Interpolatable1DFunction(object):
    def __init__(
            self,
            dep = None,
            indep = None,
            interpolator = None,
            bounds = None,
            interpolation = None,
            discretization = None,
            ):
        self.dep = dep
        self.indep = indep
        self.interpolator = interpolator
        self.interpolation = interpolation
        self.bounds = bounds
        self.discretization = discretization

    @property
    def discretization(self):
        try:
            return self._discretization
        except AttributeError:
            self._discretization = 1000
        return self._discretization

    @discretization.setter
    def discretization(self, newdiscretization):
        if newdiscretization is None:
            try:
                del self._discretization
            except AttributeError:
                pass
        else:
            self._discretization = newdiscretization

    @property
    def indep(self):
        return self._indep

    @indep.setter
    def indep(self, newindep):
        if newindep is None:
            try:
                del self._indep
            except AttributeError:
                pass
        else:
            self._indep = newindep
            if hasattr(self,"_inverse"): del self._inverse
            if hasattr(self,"_interpolator"): del self._interpolator

    @property
    def dep(self):
        return self._dep

    @dep.setter
    def dep(self, newdep):
        if newdep is None:
            try:
                del self._dep
            except AttributeError:
                pass
        else:
            self._dep = newdep
            if hasattr(self,"_inverse"): del self._inverse
            if hasattr(self,"_interpolator"): del self._interpolator

    @property
    def interpolator(self):
        try:
            return self._interpolator
        except AttributeError:
            self._interpolator = scipy.interpolate.interp1d(
                x = self.indep,
                y = self.dep,
                kind = self.interpolation,
                bounds_error = False,
                )
        return self._interpolator

    @interpolator.setter
    def interpolator(self, newinterpolator):
        if newinterpolator is None:
            if hasattr(self,'_interpolator'): del self._interpolator
        else:
            assert hasattr(newinterpolator,"__call__")
            self._interpolator = newinterpolator

    @property
    def interpolation(self):
        try:
            return self._interpolation
        except AttributeError:
            self._interpolation = "cubic"
        return self._interpolation

    @interpolation.setter
    def interpolation(self, newinterpolation):
        if newinterpolation is None:
            try:
                del self._interpolation
            except AttributeError:
                pass
        else:
            self._interpolation = newinterpolation

    @property
    def bounds(self):
        try:
            return self._bounds
        except AttributeError:
            self._bounds = [-np.Inf, np.Inf]
        return self._bounds

    @bounds.setter
    def bounds(self, newbounds):
        l,u = (None,None) if newbounds is None else newbounds
        l = -np.Inf if l is None else l
        u = np.Inf if u is None else u
        assert u>l
        self._bounds = [l,u]

    @property
    def discrete(self):
        if np.isfinite(self.bounds).all():
            l,u = self.bounds
            indep = np.linspace(l,u,self.discretization)
            dep = self(indep)
        else:
            raise ValueError("Infinite bounds and no discrete values")
        return indep, dep

    def discretize(self):
        indep, dep = self.discrete
        self.indep = indep
        self.dep = dep

    def inverse(self):
        """
        Calculate the inverse
        """
        try:
            return self._inverse
        except AttributeError:
            indep, dep = self.discrete
            dep_u, ind = np.unique(dep, return_index=True)
            indep_u = indep[ind]
            inverse = scipy.interpolate.interp1d(
                x = dep_u,
                y = indep_u,
                kind = self.interpolation,
                bounds_error = False,
                )
            self._inverse = inverse
        return self._inverse

    @staticmethod
    def discrete_integral(x, y):
        return np.sum(np.diff(x)*(y[0:-1]+y[1:])/2)

    def integral(self, x = None):
        if x is None:
            try:
                x = self.indep
                y = self.dep
            except AttributeError:
                if np.isfinite(self.bounds).all():
                    l,u = self.bounds
                    x = np.linspace(l,u,self.discretization)
                    y = self(x)
                else:
                    raise ValueError("Please specify 'x'")
        else:
            y = self(x)
        return self.discrete_integral(x,y)

    def convolve(self, kernel):
        """
        Convolve
        """
        sindep, sdep = self.discrete
        kindep, kdep = kernel.discrete
        conv = scipy.signal.convolve(sdep, kdep, mode="full") # convolve
        l,u = np.min(sindep)+np.min(kindep), np.max(sindep)+np.max(kindep)
        nindep = np.linspace(l, u, conv.size)
        conv /= self.discrete_integral(nindep, conv) # normalize
        obj = self.__class__(
            indep = nindep,
            dep = conv,
            bounds = (l,u),
            discretization = self.discretization,
            )
        return obj

    def __call__(self, dep):
        """
        Interpolate
        """
        return self.interpolator(dep)


class DistributionFunction(Interpolatable1DFunction):
    pass


class PDF(DistributionFunction):
    @property
    def cdf(self):
        """
        Calculate CDF
        """
        try:
            integral = self.interpolator.antiderivative()
            obj = CDF(
                interpolator = integral,
                bounds = self.bounds,
                discretization = self.discretization,
                )
            return obj
        except AttributeError:
            raise NotImplementedError

    @classmethod
    def from_sample(cls, *args, **kwargs):
        """
        Create from sample
        """
        cdf = CDF.from_sample(*args, **kwargs)
        return cdf.pdf

    def sample(self, *args, **kwargs):
        return self.cdf.sample(*args, **kwargs)


class CDF(DistributionFunction):
    @staticmethod
    def quantiles_from_sample(x, xmin = None, xmax = None):
        x = np.sort(x)
        if xmin is not None:
            x = np.where(x<xmin, xmin, x)
            x = np.append(xmin,x)
        if xmax is not None:
            x = np.where(x>xmax, xmax, x)
            x = np.append(x,xmax)
        xu,ind,count = np.unique(x,return_index=True,return_counts=True)
        qu=(ind+count)/x.size
        return xu, qu

    @staticmethod
    def smooth_ecdf_from_quantiles(x, quantiles):
        mi, ma = np.min(x), np.max(x)
        interp_cdf = scipy.interpolate.PchipInterpolator(
            np.concatenate([
                [mi-abs(mi)-1,mi],
                x[0:-1]+np.diff(x)/2, # midpoints
                [ma,ma+abs(ma)+1]]),
            np.concatenate([[0,0],quantiles[0:-1],[1,1]]),
            extrapolate = True
        )
        return interp_cdf

    @classmethod
    def from_sample(cls,
        sample, sample_min = None, sample_max = None,
        **kwargs):
        """
        Create from sample
        """
        v,q = cls.quantiles_from_sample(sample, sample_min, sample_max)
        ecdf = cls.smooth_ecdf_from_quantiles(v,q)
        obj = cls(
            interpolator = ecdf,
            bounds = (sample_min, sample_max),
            **kwargs,
            )
        return obj

    @property
    def pdf(self):
        """
        Calculate PDF
        """
        try:
            deriv = self.interpolator.derivative()
            obj = PDF(
                interpolator = deriv,
                bounds = self.bounds,
                discretization = self.discretization,
                )
            return obj
        except AttributeError:
            raise NotImplementedError

    def sample(self, shape = None):
        shape = self.discretization if shape is None else shape
        return self.inverse()(np.random.random(shape))

