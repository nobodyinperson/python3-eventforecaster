# system modules

# internal modules
from eventforecaster.dist import CDF

# external modules
import scipy.interpolate
import numpy as np


class RandomProcess(object):
    """
    Class to handle random processes
    """
    def __init__(self):
        pass

    @property
    def pdf(self):
        try:
            return self._pdf
        except AttributeError:
            try:
                return self.cdf.pdf
            except AttributeError:
                raise AttributeError("Impossible to determine PDF")

    @property
    def cdf(self):
        try:
            return self.cdf
        except AttributeError:
            try:
                return self.pdf.cdf
            except AttributeError:
                raise AttributeError("Impossible to determine CDF")

    @classmethod
    def from_sample(cls, sample):
        """
        Generate from sample
        """
        proc = cls()
        return proc

    def __call__(self, shape):
        """
        Sample
        """
        return self.cdf.inverse(np.random.random(shape))

