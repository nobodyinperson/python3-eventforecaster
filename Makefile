#!/usr/bin/env make -f

SETUP.PY = ./setup.py
PACKAGE_FOLDER = eventforecaster
DOCS_FOLDER = docs
DOCS_API_FOLDER = docs/source/api
INIT.PY = $(shell find $(PACKAGE_FOLDER) -maxdepth 1 -type f -name '__init__.py')

# get version from __init__.py
VERSION = $(shell perl -ne 'if (s/^.*__version__\s*=\s*"(\d+\.\d+.\d+)".*$$/$$1/g){print;exit}' $(INIT.PY))

.PHONY: all
all: $(SETUP.PY)

# write the INIT.PY version into setup.py
$(SETUP.PY): $(INIT.PY)
	perl -pi -e 's/^(.*__version__\s*=\s*")(\d+\.\d+.\d+)(".*)$$/$${1}$(VERSION)$${3}/g' $@

.PHONY: docs
docs:
	cd $(DOCS_FOLDER) && make html

.PHONY: build
build:
	$(SETUP.PY) build

.PHONY: dist
dist:
	$(SETUP.PY) sdist bdist bdist_wheel

.PHONY: upload
upload: wheel
	twine upload dist/*

.PHONY: increase-patch
increase-patch: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2,$$3,$$4+1).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)

.PHONY: increase-minor
increase-minor: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2,$$3+1,0).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)

.PHONY: increase-major
increase-major: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2+1,0,0).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)

.PHONY: tag
tag:
	git tag -f v$(VERSION)

.PHONY: setup-test
setup-test:
	$(SETUP.PY) test

.PHONY: coverage
coverage:
	coverage run --source=$(PACKAGE_FOLDER) $(SETUP.PY) test
	coverage report
	coverage html

.PHONY: clean
clean:
	rm -rf *.egg-info
	rm -rf build
	rm -rf $$(find -type d -iname '__pycache__')
	rm -f $$(find -type f -iname '*.pyc')
	(cd $(DOCS_FOLDER) && make clean)
