
Installation
============

:mod:`eventforecaster` is best installed via ``pip``::

    pip3 install --user eventforecaster
